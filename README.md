# l3m65anum

Les feuilles de td et de tp du module M65 - « Analyse numérique » de la licence 3 de mathématiques de l'université de Lille.

## Feuilles de TD

- TD1 : [[PDF](TDS/td1.pdf)] [[TeX](TDS/td1.tex)]
- TD2 : [[PDF](TDS/td2.pdf)] [[TeX](TDS/td2.tex)]
- TD3 : [[PDF](TDS/td3.pdf)] [[TeX](TDS/td3.tex)]
- TD4 : [[PDF](TDS/td4.pdf)] [[TeX](TDS/td4.tex)]
- TD5 : [[PDF](TDS/td5.pdf)] [[TeX](TDS/td5.tex)]
- TD6 : [[PDF](TDS/td6.pdf)] [[TeX](TDS/td6.tex)]

## Feuilles de TP

- TP1 : [[PDF](TPS/tp1/tp1_exercices_introduction.pdf)] [[TeX](TPS/tp1/tp1_exercices_introduction.tex)]
- TP2 : [[PDF](TPS/tp2/tp2_methodes_directes.pdf)] [[TeX](TPS/tp2/tp2_methodes_directes.tex)]
- TP3 : [[PDF](TPS/tp3/tp3_methodes_iteratives.pdf)] [[TeX](TPS/tp3/tp3_methodes_iteratives.tex)]
- TP4 : [[PDF](TPS/tp4/tp4_conditionnement.pdf)] [[TeX](TPS/tp4/tp4_conditionnement.tex)]

## Contrôles et examens

- CC : [[PDF](CC/CC1_16032013_final.pdf)] [[TeX](CC/CC1_16032013_final.tex)]
- Examen théorique : [[PDF](examTD/EXfinal_21052013.pdf)] [[TeX](examTD/EXfinal_21052013.tex)]
- Examen pratique : [[PDF](examTP/exTP_2012_2013.pdf)] [[TeX](examTP/exTP_2012_2013.tex)]
- Rattrapage théorique : [[PDF](Rattrapage/EX2s_24062013.pdf)] [[TeX](Rattrapage/EX2s_24062013.tex)]