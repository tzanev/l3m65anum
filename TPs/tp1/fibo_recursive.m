
function [ f ] = fibo_recursive( n )
% fibo_recursive( n ) : retourne le n-ème nombre de Fibonacci
%   - Cette fonction est utilisée dans l'exercice 5 du TP1
%   - Elle utilise la méthode récursive

    % on vérifie si n est un entier strictement positif
    if ( ~isnumeric(n) || n ~= round(n) || n<=0)
        error('L''argument de la fonction Fibonacci doit être un entier > 0');
    end

    % on calcule
    if n == 1
        f = 0;
    elseif n == 2
        f = 1;
    else
        f = fibo_recursive( n-1 ) + fibo_recursive( n-2 );
    end
end

