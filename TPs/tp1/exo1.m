% TP1 : Exercices d'introduction a Matlab
% ---------------------------------------
% Exercice 1
% ---------------------------------------

% nettoyage
clear; % effface les variables
close; % ferme les figures
clc; % nettoie la console

%% 
disp('--- Quastion 1-3 ----');

n=8;
x=1:2:n
y=2:2:n
z=[x y]'

%%
disp('--- Quastion 4 - methode 1 ----');
for i=1:n
    for j=1:n
        A(i,j)=n*(i-1) + j;
    end
end
% on affiche la variable A
A

%%
disp('--- Quastion 4 - methode 2 ----');

A=zeros(n);
for i=1:n^2
	A(i)=i;
end
A=A'

%%
disp('--- Quastion 4 - methode 3 ----');

% 	attention [j i]=... et non [i j]=...
[j i]=meshgrid(1:n, 1:n);
A=n*(i-1)+j

%%
disp('--- Quastion 5 ----');

% la matrice B
for i=1:n
	B(i,:)=n*(i-1)+(1:n);
end
B

% la matrice C
C=ones(n,1)*(1:n)+n*(0:n-1)'*ones(1,n)

% tests 
if(A==B)
    disp('Les matrices A et B sont identiques')
else    
    disp('Les matrices A et B sont différentes')
end    

if(A==C)
    disp('Les matrices A et C sont identiques')
else    
    disp('Les matrices A et C sont différentes')
end    

%%
disp('--- Question 6 ----');

% A*x ne marche pas (dimenssions pas compatibles)
% A*y ne marche pas (dimenssions pas compatibles)

A*z
z'*A
A'*z

%%
disp('--- Question 7 ----');

zeros(length(x),length(y))
A(x,y)
A(x,y)=zeros(length(x),length(y))
% idem A(x,y)=0
A(y,x)=zeros(length(y),length(x))
% idem A(y,x)=0

%%
disp('--- Question 8 ----');

A(~A)=-5
% idem A(A==0)=-5

%%
disp('--- Question 9 ----');

% le determinant
det(A)
% les valeurs propres
d=eig(A)
% les vecteurs et les valeurs propres
% 	Je ne sais pas comment faire pour le forcer de mettre des vecteurs
% 	propres réels pour les valeurs propres réelles
[V,D] = eig(A)
% le noyeau
kV=null(A)
% ce n'est pas exact
round(A*kV*10^14)

inv(A)
