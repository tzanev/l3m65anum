function [ f ] = fibo_formule( n )
% fibo_formule( n ) : retourne le n-ème nombre de Fibonacci
%   - Cette fonction est utilisée dans l'exercice 5 du TP1
%   - Elle utilise la formule directe

    % on vérifie si n est un entier strictement positif
    if ( ~isnumeric(n) || n ~= round(n) || n<=0)
        error('L''argument de la fonction Fibonacci doit être un entier > 0');
    end

    % on calcule
    a =[1+sqrt(5) 1-sqrt(5)]/2;
    f = round(sum([1 -1].*a.^(n-1))/sqrt(5));
end

