% TP1 : Exercices d'introduction a Matlab
% ---------------------------------------
% Exercice 4
% ---------------------------------------


% nettoyage
clear; % effface les variables
close; % ferme les figures
clc; % nettoie la console

% la fonction 'airetriangle' est definie dans airetriangle.m

% Vérification pour des triangles bien connus :
airetriangle(3,4,5)

x=sqrt(4/sqrt(3));
airetriangle(x,x,x)