function [ f ] = fibo_boucle( n )
% fibo_boucle( n ) : retourne le n-ème nombre de Fibonacci
%   - Cette fonction est utilisée dans l'exercice 5 du TP1
%   - Elle utilise une boucle pour le calcul

    % on vérifie si n est un entier strictement positif
    if ( ~isnumeric(n) || n ~= round(n) || n<=0)
        error('L''argument de la fonction Fibonacci doit être un entier > 0');
    end

    % on calcule
    g = [0,1];
    if n == 1
        f = g(1);
    elseif n == 2
        f = g(2);
    else
        for k=3:n
            g = [g(2),sum(g)];
        end
        f = g(2);
    end
end

