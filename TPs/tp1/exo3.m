% TP1 : Exercices d'introduction a Matlab
% ---------------------------------------
% Exercice 3
% ---------------------------------------


% nettoyage
clear; % effface les variables
close; % ferme les figures
clc; % nettoie la console

% dessiner la diagonale en noir, et les deux autres 'droites'
x=logspace(-5,-1,9);
loglog(x,x,'-b+',x,x.^2,'-rx',x,x.^3,'-gv');

% paramètres du graphique : axis, legend, grid
axis([10^(-5) 10^(-1) 10^(-15) 1]);
legend('h->h','h->h^2','h->h^3', 'Location','SouthEast');

% pour faire exactement comme sur la figure (à ne pas enseigner)
set(gca,'XTick',logspace(-5,-1,5),'YTick',logspace(-15,-1,8))
grid on