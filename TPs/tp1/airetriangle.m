function [ Aire ] = airetriangle( a,b,c )
% airetriangle(a,b,c) : donne la surface d'un triangle de cotés a,b,c
%   - Cette fonction est utilisée dans l'exercice 4 du TP1
	s=(a+b+c)/2;      
	Aire = (s*(s-a)*(s-b)*(s-c))^(1/2);
end

