% TP1 : Exercices d'introduction a Matlab
% ---------------------------------------
% Exercice 5
% ---------------------------------------


% nettoyage
clear; % efface les variables
close; % ferme les figures
clc; % nettoie la console

%% ---------------------------------------
% on test une des fonction qui renvoie Fibonacci : 'fibo_boucle'
fprintf('\n\nLa liste des 10 premiers nombres de Fibonacci :\n\n');
for n=1:10
    fprintf('le %2u nombre de Fibonacci est %2u\n', n, fibo_boucle(n));
end

%% ---------------------------------------
fprintf('\n\nLa liste des 10 premiers nombres de Fibonacci, avec les 3 versions :\n\n')
fibonacci = { 'fibo_recursive', 'fibo_boucle', 'fibo_formule' };
for i = 1:3
    fname = fibonacci{i};
    f = str2func(fname);
    fprintf('    avec %s :', fname);
    disp(arrayfun(f,1:10))
end

%% ---------------------------------------
fprintf('\n\nOn teste les performances des 3 versions :\n\n')
fibonacci = { 'fibo_recursive', 'fibo_boucle', 'fibo_formule' };
for i = 1:3
    fname = fibonacci{i};
    f = str2func(fname);
    fprintf('    avec %s :', fname);
    tic;
    arrayfun(f,1:21);
    t = toc;
    fprintf('%.10f secondes\n\n', t);
end


