% TP1 : Exercices d'introduction a Matlab
% ---------------------------------------
% Exercice 6
% ---------------------------------------


% nettoyage
clear; % effface les variables
close; % ferme les figures
clc; % nettoie la console

% le nombre de pas
n = 10;
%% 
disp('--- Quastion 1 ---')

disp('La matrice du laplacien discret :')
A = diag(-1*ones(1,n-1),1)+diag(-1*ones(1,n-1),-1)+2*eye(n)

%% 
disp('--- Quastion 2 ---')

disp('La même matrice en utilisant "gallery" :')
A2 = gallery('tridiag',n,-1,2,-1)
disp('On obtien une matrice creuse. On peut la remplir avec "full" :')
full(A2)


%% 
disp('--- Quastion 3 ---')

disp('--- a)')

c = inline('x','x')
f = inline('(pi^2+x).*sin(pi*x)','x')
% où f = inline('(pi^2+x)*sin(pi*x)','x'); 
% mais après il va falloir faire arrayfun(@(x)f(x),X)

%%
disp('--- b),c)')

% le pas de discretisation
h = 1/(1+n)
% les vecteurs
X = h*(1:n)
F = f(X)

%%
disp('--- d)')

% les matrices
D = diag(c(X));
M = A/(h^2)+D 

%%
disp('--- e)')

disp('la solution approché est :');
U=M\F'

%%
disp('--- f)')

% on dessine 
%   - la solution exacte 
%   - et la solution approché avec 0 et 1 rajoutés aux vecteurs
x=0:0.01:1;
u = inline('sin(pi*x)','x');
plot(x,u(x),'b', [0,X,1],[0;U;0],'or');
legend('la solution exacte','la solution approchée','Location','South');

% une autre methode qui utilise fplot et hold
% fplot(u,[0 1])
% hold on
% plot([0,X,1],[0;U;0],'or')
% hold off
