% TP1 : Exercices d'introduction a Matlab
% ---------------------------------------
% Exercice 2
% ---------------------------------------


% nettoyage
clear; % effface les variables
close; % ferme les figures
clc; % nettoie la console

% variables et fonctions
t=linspace(0,2*pi,50);
x=cos(t); y=sin(t);
s=-2.5:0.1:2.5;
% dessiner l'ellipse, le cercle et l'arctan
plot(2*x,y,'-or', x,y,'-+b', s,atan(s),'-k');

% paramétrage du graphe : axis, title, legend
axis([-2.5 2.5 -2.5 2.5])
title('Differentes courbes');
legend('ellipse : x(t)=2*cos(t), y(t)=sin(t)','cercle : x(t)=cos(t), y(t)=sin(t)','y=arctan(x)', 'Location','SouthWest');



