% TP2 : Méthodes directes de résolution de systèmes linéaires
% ---------------------------------------
% Partie 2. Factorisation LU avec permutation
% ---------------------------------------

% nettoyage
clear; % efface les variables
close; % ferme les figures
clc; % nettoie la console

%% ----------------------------------
fprintf('\n--- Question 1 ----\n\n');
fprintf('La commande A([i j],:)=A([j i],:) échange les lignes i et j.\n\n');
fprintf('Cette opération appliqué à l''identité donne la matrice de permutation\n de la transposition (i,j).\n');

%% ----------------------------------
fprintf('\n--- Question 2 ----\n\n');

fprintf('La matrice A1 et le max de la première colonne:\n')
A1 = [2 -1 0 1 ; 1 1 2 1; -4 -1 -1 -2; 1 1 3 1]
[a r]=max(abs(A1(:,1)));
fprintf('a=%d r=%d\n\n',a,r)

% -----------
fprintf('La matrice A2 et le max de la première colonne:\n')
A2 = [2 1 0 4; 4 1 -2 8; -4 -2 3 -7;0 3 -12 -1]
[a r]=max(abs(A2(:,1)));
fprintf('a=%d r=%d\n',a,r)

%% ----------------------------------
fprintf('\n--- Question 3 ----\n\n');
fprintf('Voir le fichier factolu_avec_perm.m .\n\n');

%% ----------------------------------
fprintf('\n--- Question 4 ----\n\n');

fprintf('La décomposition PA=LU pour A1 :\n')
[L,U,P]=factolu_avec_perm(A1)
fprintf('On vérifie si P*A1 = L*U :\n')
compare(P*A1,L*U)

%% -----------
fprintf('On compare avec la méthode factolu pour A1:\n\n')
[L1,U1]=factolu(A1)
fprintf('L==L1 ?\n')
disp([num2str(L,1) ;'    ';num2str(L1,1)])
compare(L,L1)

fprintf('U==U1 ?\n')
disp([num2str(L,1) ;'    ';num2str(L1,1)])
compare(U,U1)

fprintf('C''est normal, la matrice P n''est pas identité !\n\n')

%% -----------
fprintf('On compare avec la méthode lu de MatLab pour A1:\n\n')
[L1 U1 P1] = lu(A1);
fprintf('L==L1 ?\n  ')
compare(L,L1)
fprintf('U==U1 ?\n  ')
compare(U,U1)
fprintf('P==P1 ?\n  ')
compare(P,P1)

%% ----------------------------------
fprintf('\n--- Question 5 ----\n\n');
fprintf('Résolution de systèmes AX=Y avec resoudre_systeme_plu: \n\n');

disp('Une matrice au hasard :')
A = round(5-10*rand(4,4))

disp('Un vecteur au hasard :')
Y = round(5-10*rand(4,1))

disp('La solution de l''équation AX=Y est :')
resoudre_systeme_plu(A,Y)

disp('On compare notre méthode et celle de Matlab pour trouver X t.q. AX=Y :')
compare(resoudre_systeme_plu(A,Y), A\Y)

