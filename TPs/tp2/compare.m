function [ m ] = compare(A, B, tolerance = 1e-10)
% meme(A,B) - teste si les deux matrices sont identiques à 'tolerance' près.

    if (abs(A -B) < tolerance)
        fprintf('Les deux matrices sont identiques à %.0e près.\n\n',tolerance)
    else
        fprintf('Les deux matrices sont différents à plus de %.0e près.\n\n',tolerance)
    end
end

