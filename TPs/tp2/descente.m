function [X] = descente(L,b)
%descente(L,b) - renvoie le vecteur colonne X solution de LX=b
%   Cette fonction résout le système triangulaire inférieur LX=b
%   par la méthode de la descente.
%   L doit donc être triangulaire inférieure, inversible.

    % ---------- vérification des paramètres
    [n m]=size(L);
    [k l]=size(b);
    if (n~=m || m~=k || l~=1)
        error('Les matrices ne sont pas de la bonne dimension.');
    end

    if (any(any(L ~= tril(L))))
        error('La matrice L n''est pas triangulaire inférieure.');
    end

    % ---------- calcul de X
    X=zeros(n,1);
    for i=1:n
        if L(i,i) % normalement ici on doit vérifier L(i,i) > eps
            if i==1
                X(1)=b(1)/L(1,1);
            else
                X(i)=(b(i)-L(i,:)*X)/L(i,i); % que la partie L(i,1:i-1) compte
            end
        else
            error('La matrice L n''est pas inversible.');
        end
    end
end

