function [X] = remontee(U,b)
%remontee(L,b) - renvoie le vecteur colonne X solution de UX=b
%   Cette fonction résout le système triangulaire supérieur UX=b
%   par la méthode de la remontée.
%   U doit donc être triangulaire supérieure, inversible.

    % ---------- vérification des paramètres
    [n m]=size(U);
    [k l]=size(b);
    if (n~=m || m~=k || l~=1)
        error('Les matrices ne sont pas de la bonne dimension.');
    end

    if (any(any(U ~= triu(U))))
        error('La matrice U n''est pas triangulaire supérieure.');
    end

    % ---------- calcul de X
    X=zeros(n,1);
    for i=n:-1:1
        if U(i,i)
            if i==n
                X(n)=b(n)/U(n,n);
            else
                X(i)=(b(i)-U(i,:)*X)/U(i,i); % que la partie U(i,i+1:n) compte
            end
        else
            error('La matrice U n''est pas inversible.');
        end
    end
end

