function X=resoudre_systeme_plu(A,b)
% resoudresystemelu(A,b) - renvoie le vecteur colonne X solution de AX=b
%   Cette fonction résout le système triangulaire inférieur AX=b
%   si la matrice A est inversible

    [L U P] = factolu_avec_perm(A);  % LU = PA
    % AX = b <=> PAX = Pb  <=> LUX = Pb
    Z = descente(L,P*b);   % LZ = Pb
    % Si la matrice A n'est pas inversible la methode remontee va afficher le message d'erreur
    % car A inversible <=> U inversible
    X = remontee(U,Z);   % UX = Z => (LUX = Pb) => PAX = Pb => AX = b
end

