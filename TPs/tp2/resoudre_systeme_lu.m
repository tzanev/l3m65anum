function X=resoudre_systeme_lu(A,b)
% resoudresystemelu(A,b) - renvoie le vecteur colonne X solution de AX=b
%   Cette fonction résout le système triangulaire inférieur AX=b
%   si la fonction A admet la décomposition A=LU

    [L U] = factolu(A);  % LU = A
    Z = descente(L,b);   % LZ = b
    X = remontee(U,Z);   % UX = Z => (LUX = b) => AX = b
end

