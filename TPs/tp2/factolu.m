function [L,U]= factolu(A)
%factolu(A) - calcule la décomposition LU de A
%   Pour que la décomposition LU de A puisse être calculée
%   il faut que les sous-matrices principales de A soit inversibles.

    % ---------- vérification des paramètres
    [n m]=size(A);
    if (n~=m)
        error('Le matrice A n''est pas carrée.');
    end

    % ---------- calcul de L et de U
    L = 1;
    U = A(1,1);
    for k = 1:(n-1)
        if U(k,k)
            X = descente(L,A(1:k,k+1)); % LX=A(1:k,k+1)
            Y = descente(U',A(k+1,1:k)')'; % YU=A(k+1,1:k)
            z = A(k+1,k+1) - Y*X;
            L = [
                    L  zeros(k,1)
                    Y       1
                ];
            U = [
                        U       X
                    zeros(1,k)  z
                ];
        else
            error('La décomposition LU est impossible.');
        end
    end
end

