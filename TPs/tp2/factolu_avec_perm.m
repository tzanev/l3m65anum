function [L,U,P]=factolu_avec_perm(A)
%factolu_avec_perm(A) - calcule la décomposition LU de A avec permutation
%   Cette fonction renvoie trois matrices L, U et P, avec P matrice de
%   permutation et L et U telles que LU=PA soit soit la décomposition LU de
%   PA.

    [n k]= size(A);
    if n ~= k
        error('La matrice A doit être carrée')
    end

    % calcul des L, U et P par récurrence
    if n == 1
        P=1; L=1; U=A;
        return
    end
    [a r] = max(abs(A(:,1)));
    A([1 r],:) = A([r 1],:); % on échange les lignes 1 et r de A
    a = A(1,1); % on récupère le bon signe
    if a == 0
        m=zeros(n-1,1);
    else
        m= A(2:n,1)/a;
    end
    v = A(1,2:n);
    C = A(2:n, 2:n) - m*v; % B = C+m*v
    [LL,UU,PP] = factolu_avec_perm(C);
    L = [
             1      zeros(1,n-1)
            PP*m        LL
        ];
    U = [
               a            v
            zeros(n-1,1)    UU
        ];
    P = blkdiag(1,PP);
    % on échange les colonnes 1 et r,
    % donc on multiplie par P_{1,r} à droite
    P(:,[1 r]) = P(:,[r 1]);
end

