% TP2 : Méthodes directes de résolution de systèmes linéaires
% ---------------------------------------
% Partie 1. Factorisation LU
% ---------------------------------------

% nettoyage
clear; % efface les variables
close; % ferme les figures
clc; % nettoie la console

%% ----------------------------------
fprintf('\n--- Question 1 ----\n\n');

fprintf('Les commandes sont A(1:k,k+1) et A(k+1,1:k)\n\n');

%% ----------------------------------
fprintf('\n--- Question 2 ----\n\n');
fprintf('Voir les fichiers descente.m et remontee.m.\n\n');

%% ----------------------------------
fprintf('\n--- Question 3 ----\n\n');
fprintf('Test de la descente : \n\n');

disp('Une matrice triangulaire inférieure au hasard')
A=round(50-100*rand(7));
A = tril(A)
disp('Un vecteur colonne au hasard')
Y = round(50-100*rand(7,1))
disp('La solution de l''équation AZ=Y')
Z = descente(A,Y)
disp('Des différences mineures avec la méthode "\" de Matlab (dû à des arrondis) :')
(A\Y == Z)'
disp('Mais sont-elles identiques à 0.0000000001 près ?')
compare(A\Y,Z)

%% ------------
fprintf('Test de la remontée : \n\n');

disp('La transposée de A, qui est triangulaire supérieure :')
A = A'
disp('La solution de l''équation AZ=Y :')
Z = remontee(A,Y)
disp('Les différences mineures avec la méthode "\" de Matlab sont invisibles à l''impression :')
[(A\Y)';Z']

%% ----------------------------------
fprintf('\n--- Question 4 ----\n\n');
fprintf('Voir le fichier factolu.m.\n\n');

%% ----------------------------------
fprintf('\n--- Question 5 ----\n\n');
fprintf('Test de la factorisation LU par factolu: \n\n');

disp('Une matrice au hasard avec des coefficients entiers entre -50 et 50,')
disp('est normalement décomposable en LU :')
A=round(50-100*rand(4))
disp('Le résultat :')
[L U]=factolu(A)
disp('On compare LU avec A :')
compare(L*U,A)

disp('La méthode lu de MatLab :')
[L2 U2 P2]=lu(A) % je ne sais pas comment forcer P2=Id !!!!

%% ----------------------------------
fprintf('\n--- Question 6 ----\n\n');
fprintf('Résolution de systèmes AX=Y avec resoudre_systeme_lu: \n\n');

disp('Un vecteur au hasard :')
Y = round(50-100*rand(4,1))

disp('On compare notre méthode et celle de Matlab  pour trouver X t.q. AX=Y :')
compare(resoudre_systeme_lu(A,Y), A\Y)


