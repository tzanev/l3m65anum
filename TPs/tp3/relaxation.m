function [x,nbit]=relaxation(A,b,omega,eps)
%relaxation(A,b,omega,eps) - calcule la solution de Ax=b par la méthode de la relaxation
%   A est une matrice carrée, b est un vecteur colonne,
%   omega la paramètre de relaxation et eps est la précision demandée.
%   Le nombre maximal d'itérations est fixé à 10000.

    % les conditions de fin d'itération
    nbitmax=1e4;
    errmax = eps*norm(b,inf);

    % la matrice itérative
    M = diag(diag(A))/omega + tril(A,-1);

    % le point de départ x=(0,...,0)
    [m n] = size(A);
    x = zeros(m,1);

    % l'itération
    for nbit = 1:nbitmax
        err = A*x-b;
        if ( norm(err,inf) < errmax )
            break;
        end
        x = x-M\err;
    end
end

