function [x,nbit]=jacobi(A,b,eps)
%jacobi(A,b,eps) - calcule la solution de Ax=b par la méthode de Jacobi
%   A est une matrice carrée, b est un vecteur colonne,
%   et eps est la précision demandée.
%   Le nombre maximal d'itérations est fixé à 10000.

    % les conditions de fin d'itération
    nbitmax=1e4;
    errmax = eps*norm(b,inf);

    % la matrice itérative
    D = diag(diag(A));

    % le point de départ x=(0,...,0)
    [m n] = size(A);
    x = zeros(m,1);

    % l'itération
    % il est plus logique de la faire avec while,
    % mais plus court avec for
    for nbit = 1:nbitmax
        err = A*x-b;
        if ( norm(err,inf) < errmax )
            break;
        end
        x = x-D\err;
    end
end

