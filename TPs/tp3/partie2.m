% TP3 : Méthodes itératives de résolution de systèmes linéaires
% ---------------------------------------
% Partie 2. Comparaison d'efficacité sur une matrice bien connue
% ---------------------------------------


% nettoyage
clear; % efface les variables
close; % ferme les figures
clc; % nettoie la console

%% ----------------------------------
fprintf('\n--- Question 1 ----\n\n');
disp('Pour la relaxation et Gauss-Seidel : La matrice est symétrique définie positive')
disp('Pour Jacobi : La matrice est tridiagonale et Gauss-Seidel converge')

%% ----------------------------------
fprintf('\n--- Question 2 ----\n\n');

disp('La matrice du laplacien 1D :')
laplacien = inline('2*eye(n)-diag(ones(n-1,1),1)-diag(ones(n-1,1),-1)','n');
disp(laplacien(5));

disp('Levecteur de départ :')
secmem = inline('[1;zeros(n-2,1);1]','n');
disp(secmem(5));

%% ----------------------------------
fprintf('\n--- Question 3 et 4 ----\n\n');
disp('Comparaison de Jacobi avec Gauss-Seidel')

for n = [10,40]
    A = laplacien(n); % la matrice du laplacien 1D de taille n
    b = secmem(n); % le vecteur second membre (1,0,0,...,0,1)
    % on fait varier la précision
    for e=[1e-6,1e-10]
        [x1 nb1] = jacobi(A,b,e);
        [x2 nb2] = relaxation(A,b,1,e);
        fprintf('Pour epsilon = %e et n = %u\n',e,n);
        fprintf(' - Jacobi en %u itérations \n',nb1);
        fprintf(' - Gauss-Seidel en %u itérations \n\n',nb2);
    end
end

%% ----------------------------------
fprintf('\n--- Question 5 ----\n\n');
disp('Le nombre d''itérations en fonction de omega')

e=1e-6; % la précision
dim = [10, 40]; % les tailles des matrices
for k=[1, 2]
    n = dim(k);
    A = laplacien(n); b = secmem(n);
    intervalle = linspace(1,1.99,100); % la subdivision de l'intervalle
    iter = []; % le vecteur qui va contenir le nombre d'itérations
    for o = intervalle
        [x nb] = relaxation(A,b,o,e);
        iter = [iter,nb]; % on rajoute le nombre d'itérations pour ce parametre o(mega)
    end
    % on divise l'ecran en 3x2 pour pouvoir y mettre aussi
    % les graphiques des questions suivantes
    subplot(3,2,k); % on selectionne la partie (1 ou 2) pour placer le graphique
    plot(intervalle, iter,'-r'); % on rajoute le graphique
    title(sprintf('Nombre d''iteration pour n=%u',n));
end

%% ----------------------------------
fprintf('\n--- Question 6 ----\n\n');
disp('Le rayon spectral en fonction de omega')

e=1e-6; % la précision
dim = [10, 40]; % les tailles des matrices
for k=[1, 2]
    n = dim(k);
    A = laplacien(n);
    intervalle = linspace(1,1.99,100);
    rho=[];
    for o = intervalle
        J = diag(diag(A))/o + tril(A,-1); % la matrice da relaxation pour omega = o
        I = eye(n) - inv(J)*A; % la matrice d'itération
        rho = [ rho, norm(eig(I),inf) ]; % on rajoute le rayon spectral de I pour o(mega)
    end;
    subplot(3,2,2+k); % on selectionne la partie (3 ou 4) où placer le graphique
    plot(intervalle, rho,'-r');
    title(sprintf('Rayon spectral pour n=%u',n));
end

%% ----------------------------------
fprintf('\n--- Question 7 ----\n\n');
disp('Jacobi, Gauss-Seidel et relaxation avec omega = 1.7')

e = 1e-6; % la précision
taille = [3:50]; % la taille de la matrice du laplacien 1D (de 3 à 50)
ja=[]; gs=[]; re=[]; % les vecteurs qui vont contenir le nombre d'itération
for n = taille
    A = laplacien(n); b = secmem(n);
    [x nb] = jacobi(A,b,e); ja = [ja,nb]; % on rajoute le nombre d'itérations
    [x nb] = relaxation(A,b,1,e); gs = [gs,nb]; % on rajoute le nombre d'itérations
    [x nb] = relaxation(A,b,1.7,e); re = [re,nb]; % on rajoute le nombre d'itérations
end
subplot(3,2,[5,6]); % on placer le graphique sur deux positions (5 et 6)
loglog(taille,ja,'-r',taille,gs,'-g',taille,re,'-b');
title('Jacobi, Gauss-Seidel et relaxation 1.7');
legend('Jacobi','Gauss-Seidel','relaxation (\omega = 1.7)', 'Location','NorthWest');
