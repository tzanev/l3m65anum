% TP3 : Méthodes itératives de résolution de systèmes linéaires
% ---------------------------------------
% Partie 1. Programmation des méthodes
% ---------------------------------------

% nettoyage
clear; % efface les variables
close; % ferme les figures
clc; % nettoie la console

e = 0.01; % la précision
A = [10 -1 4; 2 5 -2; -5 1 7];
b = 5*rand(3,1); % et un vecteur colone

%% ----------------------------------
fprintf('\n\n---- Question 1 : Jacobi\n\n');

% on teste
[x nbit] = jacobi(A,b,e);

% et on vérifie l'erreur
erreur = norm(A*x-b, inf)/norm(b,inf);

fprintf('Après %u itérations avec une précision de %e on trouve :\n\n',nbit,erreur);
disp(x');

% on compare avec la solution exacte
y = A\b;
fprintf('\nComparaison avec la solution exacte :\n\n');
disp([x y]);

%% ----------------------------------
fprintf('\n\n---- Question 2 : Relaxation\n\n');

% on teste
for o=[0.8 1 1.2]
    [x nbit] = relaxation(A,b,o,e);
    fprintf('Pour omega = %.1f le nombre d''opérations est = %u\n',o,nbit);
end

%% ----------------------------------
fprintf('\n\n---- Question 3 : Comparaison\n\n');

% le matrice et le veteurs pour le test 1
A=[1  2 -2; 1 1 1; 2 2 1];
b=[1;0;-1];

fprintf('\n Test 1\n\n');
[x1 nb1] = jacobi(A,b,e);
[x2 nb2] = relaxation(A,b,1,e);
fprintf('\n Nombre d''itérations : %u (Jacobi) et %u (Gauss-Seidel)\n\n', nb1, nb2);

% le matrice et le veteurs pour le test 2
A=[2 -1 1; 2 2 2; -1 -1 2];
b=[1;-1;-1];

fprintf('\n Test 2\n\n');
[x1 nb1] = jacobi(A,b,e);
[x2 nb2] = relaxation(A,b,1,e);
fprintf('\n Nombre d''itérations : %u (Jacobi) et %u (Gauss-Seidel)\n\n', nb1, nb2);

fprintf('\nLes matrices A ne sont ni symétriques ni à diagonal strictement dominant.\n\n');
