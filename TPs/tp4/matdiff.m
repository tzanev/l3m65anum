function [ D ] = matdiff( p,d1,d2 )

    % Pour produire une matrice creuse on peut faire
    % D=gallery('tridiag',[-d1*ones(1,p), -d2*ones(1,p)],[2*d1*ones(1,p), d1+d2, 2*d2*ones(1,p)],[-d1*ones(1,p), -d2*ones(1,p)]);

    D = blkdiag(d1*matlap(p),d1+d2,d2*matlap(p));
    D(p,p+1)=-d1;D(p+1,p)=-d1;
    D(p+1,p+2)=-d2;D(p+2,p+1)=-d2;
end
