function [ x, nbiter ] = grad_pas_constant_opt( A, b, errmax, nbitmax )
    % la tolerence de fin d'itération
    rmax = errmax*norm(b,2);

    % calcul du alpha (a) optimal
    sp = eig(A);
    a = 2/(min(sp)+max(sp));

    % le point de départ x=(0,...,0)
    [m n] = size(A);
    x = zeros(m,1);

    % l'itération
    for nbiter = 1:nbitmax
        r = b-A*x;
        if ( norm(r,2) < rmax )
            break;
        else
            x = x+a*r;
        end
    end
end

