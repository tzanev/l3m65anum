function [ x, nbiter ] = grad_pas_opt( A, b, errmax, nbitmax )
    % la tolerence de fin d'itération
    rmax = errmax*norm(b,2);

    % le point de départ x=(0,...,0)
    [m n] = size(A);
    x = zeros(m,1);

    % l'itération
    for nbiter = 1:nbitmax
        r = b-A*x;
        if ( norm(r,2) < rmax )
            break;
        else
            a = (r'*r)/(r'*A*r);
            x = x+a*r;
        end
    end
end

