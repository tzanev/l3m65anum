function [ x, nbiter ] = grad_precond_pas_opt( A, M, b, errmax, nbitmax )
    % la tolerence de fin d'itération
    rmax = errmax*norm(b,2);

    % le point de départ x=(0,...,0)
    [m n] = size(A);
    x = zeros(m,1);

    % l'itération
    r = b - A*x;
    for nbiter = 1:nbitmax
        if ( norm(r,2) < rmax )
            break;
        else
            z = M\r;
            Az = A*z; % valeur utilisée deux fois
            a = (r'*z)/(z'*Az);
            x = x + a*z;
            r = r - a*Az;
        end
    end

end

