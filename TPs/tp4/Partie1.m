% TP4 : Conditionnement et méthodes de gradient
% ---------------------------------------
% Partie 1. Conditionnement de matrices particulières
% ---------------------------------------

% nettoyage
clear;
close;
clc;

% ------------ Quastion 1 (Matrice de Hilbert) -----------------

disp(' 1.1.1) On trouve l''intégral du carré d''un polynôme de degré n.');
disp(' 1.1.2) Voir le graphique.');
n = 10; % la taille maximale
t = 1:n; % le vecteur des tailles
c = zeros(1,n); % le vecteur qui va contenir le log du conditionnemen
for i = 1:n
    c(i) = cond(hilb(i));
end
semilogy(t,c);
title('Matrice de Hilbert : Log(conditionnement) / taille');
fprintf(' 1.1.3) ');
fprintf('On a l''impression que cond ~ C^n avec C proche de 10.\n');
%Par contre si on fait n=100, on constate que à partir de 40 ce n''est plus vrai ...

% ------------ Quastion 2 (Diffusion) -----------------

disp(' 1.2.1) Si d1=d2=1 c''est la matrice du laplacien discret.');
disp(' 1.2.2) Le méthode est identique à celle du laplacien.');
disp(' 1.2.3) Voir le fichier matdiff.m (qui utilise matlap.m)');
disp(' 1.2.4) Voir le graphique en loglog, car cond~Cn²');
figure; % creer une nouvelle figure

n = 8; % le p maximale est 2^n
p = 2.^(1:n); % le vecteur des tailles
c = zeros(1,n); % le vecteur qui va contenir le conditionnemen
for i = 1:n
    c(i) = cond(matdiff(p(i),1,1));
end
subplot(2,1,1);
loglog(2*p+1,c);
title('Diffusion : conditionnement / taille');

disp(' 1.2.5) Voir le graphique.');

n = 5; % le d2 max est 10^n
d = 10.^(0:n); % le d2
c = zeros(1,n); % le vecteur qui va contenir le conditionnemen
for i = 1:(n+1)
    c(i) = cond(matdiff(20,1,d(i)));
end
subplot(2,1,2);
plot(d,c);
% le conditionnement de la matrice du diffusion dépend de d1/d2
title('Diffusion : conditionnement / d1:d2');
