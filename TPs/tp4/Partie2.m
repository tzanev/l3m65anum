% TP4 : Conditionnement et méthodes de gradient
% ---------------------------------------
% Partie 2. Méthodes de gradient
% ---------------------------------------

% nettoyage
clear;
close;
clc;

nbitmax = 1e4; % nombre max d'itérations
errmax = 1e-3; % la précision

% ===============================
disp(' 2.1) Voir le fichier grad_pas_constant_opt.m');
disp(' 2.2) Voir le fichier grad_pas_opt.m');

% ===============================
disp(' ---- quastion 2.3 ---- ');

p = 10;
A = matdiff(p,1,1);
b = ones(2*p+1,1);

[ x, nbiter ] = grad_pas_constant_opt( A,b,errmax, nbitmax );
fprintf('Matrice de diffusion avec la méthode à pas CONSTANT optimal :\n   %u nombre d''itérations, pour une précision de %.4f \n\n',nbiter, norm(A*x-b,2));


[ x, nbiter ] = grad_pas_opt( A,b,errmax, nbitmax );
fprintf('Matrice de diffusion avec la méthode à pas VARIABLE optimal :\n   %u nombre d''itérations, pour une précision de %.4f \n\n',nbiter, norm(A*x-b,2));

% ===============================
disp(' ---- quastion 2.4 ---- ');
disp('Voir figure 1.');

n = 10; % la taille maximale
t = 1:n; % le vecteur des tailles
c = zeros(1,n); % le vecteur qui va contenir le log du conditionnemen
nitc = zeros(1,n); % le vecteur qui va contenir le nombre d'itérations (pas constant)
nitv = zeros(1,n); % le vecteur qui va contenir le nombre d'itérations (pas variable)
for i = 1:n
    H = hilb(i);
    % conditionnement
    c(i) = cond(H);
    % nombre d'itérations
    b = ones(i,1);
    [x nitc(i)] = grad_pas_constant_opt( H, b, errmax, nbitmax );
    [x nitv(i)] = grad_pas_opt( H, b, errmax, nbitmax );
end
subplot(2,1,1);
semilogy(t,c,'o-g');
title('Hilbert, Log(conditionnement)/tailleen');
subplot(2,1,2);
plot(t,nitc, 'o-b',t,nitv, 'o-r');
title('Hilbert, itérations / taille');

figure;
% ===============================
disp(' ---- quastion 2.5a ---- ');

n = 10; % le p maximale est 3*n
p = 3*(1:n);
t = 2*p+1; % le vecteur des tailles
c = zeros(1,n); % le vecteur qui va contenir le conditionnemen
for i=1:n
    A = matdiff(p(i),1,5);
    c(i)=cond(A);
    b=ones(2*p(i)+1,1);
    [x nitc(i)] = grad_pas_constant_opt( A, b, errmax, nbitmax );
    [x nitv(i)] = grad_pas_opt( A, b, errmax, nbitmax );
end
subplot(2,2,1);
loglog(2*p+1,c);
title('Diffusion, conditionnement/taille');
subplot(2,2,3);
loglog(t,nitc, 'o-b',t,nitv, 'o-r');
title('Diffusion, itérations/taille');

% ===============================
disp(' ---- quastion 2.5b ---- ');
disp('Voir figure 2.');

% ----------------------------
n=10; % le d2 max est n
p = 20;
d=(1:n); % le d2
b=ones(2*p+1,1);
c=zeros(1,n); % le vecteur qui va contenir le conditionnemen
for i=1:n
    A = matdiff(p,1,d(i));
    c(i)=cond(A);
    [x nitc(i)] = grad_pas_constant_opt( A, b, errmax, nbitmax );
    [x nitv(i)] = grad_pas_opt( A, b, errmax, nbitmax );
end
subplot(2,2,2);
plot(d,c);
title('Diffusion, conditionnement/d2');
subplot(2,2,4);
plot(d,nitc, 'o-b',d,nitv, 'o-r');
title('Diffusion, itérations/d2');


