% nettoyage
clear;
close all;
clc;

nbitmax = 1e4; % nombre max d'itérations
errmax = 1e-3; % la précision

% ===============================
disp(' ---- Le laplacien ---- ');

p=10;
A = matdiff(p,1,1);
b=ones(2*p+1,1);

[ x, nbiter ] = grad_pas_opt( A,b,errmax, nbitmax );
fprintf('Matrice du laplacien SANS préconditionnement :\n   %u nombre d''itérations, pour une précision de %.4f \n\n',nbiter, norm(A*x-b,2));

[ x, nbiter ] = grad_precond_pas_opt( A, diag(diag(A)), b, errmax, nbitmax );
fprintf('Matrice du laplacien AVEC préconditionnement diagonal :\n   %u nombre d''itérations, pour une précision de %.4f \n\n',nbiter, norm(A*x-b,2));

[ x, nbiter ] = grad_precond_pas_opt( A, tril(A), b, errmax, nbitmax );
fprintf('Matrice du laplacien AVEC préconditionnement triangulaire :\n   %u nombre d''itérations, pour une précision de %.4f \n\n',nbiter, norm(A*x-b,2));

fprintf('Le preconditionnement diagonal n''a pas d''effet dans le cas d''une matrice à diagonal constant');

% ===============================
disp(' ---- Diffusion ---- ');

n = 10; % le p maximale est 3*n
p = 3*(1:n);
t = 2*p+1; % le vecteur des tailles
for i=1:n
    A = matdiff(p(i),1,5);
    b=ones(2*p(i)+1,1);
    [x nits(i)] = grad_pas_opt( A, b, errmax, nbitmax );
    [x nita(i)] = grad_precond_pas_opt( A, tril(A), b, errmax, nbitmax );
end
plot(t,nits, 'o-b',t,nita, 'o-r');
title('Diffusion, sans et avec préconditionnement / taille');

% ----------------------------
figure;
n=10; % le d2 max est n
p = 20;
d=(1:n); % le d2
b=ones(2*p+1,1);
for i=1:n
    A = matdiff(p,1,d(i));
    [x nits(i)] = grad_pas_opt( A, b, errmax, nbitmax );
    [x nita(i)] = grad_precond_pas_opt( A, tril(A), b, errmax, nbitmax );
end
plot(d,nits, 'o-b',d,nita, 'o-r');
title('Diffusion, sans et avec préconditionnement / d2');


% ===============================
disp(' ---- Hilbert ---- ');

figure;
n=10; % la taille maximale
t=1:n; % le vecteur des tailles
nits=zeros(1,n); % le vecteur qui va contenir le nombre d'itérations (sans cond.)
nita=zeros(1,n); % le vecteur qui va contenir le nombre d'itérations (avec cond.)
for i=1:n
    H = hilb(i);
    % nombre d'itérations
    b=ones(i,1);
    [x nits(i)] = grad_pas_opt( H, b, errmax, nbitmax );
    [x nita(i)] = grad_precond_pas_opt( H, tril(H), b, errmax, nbitmax );
end
plot(t,nits, 'o-b',t,nita, 'o-r');
title('Hilbert, itérations / taille');
