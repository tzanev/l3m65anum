%% -------- exo 1 ---------------
if ( M' + N == M + N' )
    disp('M^t + N est symétrique')
end

%% -------- exo 2 ---------------
D = diag(diag(A));
J = eye(size(A))-D\A;

%% -------- exo 3 ---------------
a*ones(n)+(b-a)*eye(n)

%% -------- exo 4 ---------------

% 1 : on attribue à la variable 'nb_iter_max' la veleur 10000
% 2 : on attribue à la variable 'err_max' la veleur 0,00001
% 3 : on boucle au plus 10000 fois (si le 'break' ne se produit pas avant).
% 4 : on verifie si le vecteur Mx-Nx-b est suffisament petit (moins que 0,00001. S'il est 0, x est la solution de (M-N)x=b)
% 5 : si c'est le cas on termine la boucle d'itération
% 6 : fin du 'if'
% 7 : on calcule la valeur suivante de la suite itérative Mx_{k+1} = Nx_{k} + b
% 8 : fin de la boucle
