% exo 2
% ------------
err_max = 1e-4;
iter_max = 1e4;
k = 0;
r = b-A*x;
while ( r > err_max && k < iter_max)
    x = x - a*M\r;
    r = b-A*x;
    k = k+1;
end

% exo 3
% ------------
minorant = max(diag(B))/min(diag(B));

% exo 4
% ------------
abs(A') % les valeurs absolue de la transposée
sum(abs(A')) % la somme des valeurs absolues des lignes de A (=colonnes de A')
max(sum(abs(A'))) % la norme |||A|||_\infty


% exo 5
% ------------
t = 1:100;  % les tailles de la matrice du laplacien discret
c = zeros(length(t)); % le vecteur qui va contenir les conditionnements
for n = t % n varie de 1 à 100
    A = diag(-1*ones(1,n-1),1) + ...
        diag(-1*ones(1,n-1),-1) + ...
        2*eye(n);  % le laplacien de taille n
    c(n)=cond(A); % son conditionnement est stocker dans c
end
loglog(t,c); % on affiche le log du conditionnement en fonction du log de la taille
% le graphique doit être proche d'une droite linéaire de pente 2
