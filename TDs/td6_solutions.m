%   exo 2
% --------------
%  pour optimiser on peut remplacer A par A' si size(A,1) < size(A,2)
e = eig(A'*A)
e = sqrt(sort(e(e>eps))) % les nombres plus petits que eps sont considérés comme 0

%   exo 3
% --------------
if (norm(v) > eps) % si v n'est pas 0
    pseudo_inverse = v'/norm(v)^2;
else
    pseudo_inverse = v';
end

%   exo 5
% --------------

% 1 ) A est une matrice, b est un vecteur colonne et y est un nombres
% 2 )
%       ligne 1 : declaration de la fonction
%       ligne 2 : x est une solution au sens des moindres carrés du système A*x=b
%       ligne 3 : erreur dans la solution au sens des moindres carrés
% 3 ) Calcule la distance entre Im(A) et b = erreur dans la solution au sens des moindres carrés
% 4 ) d(A,b) == 0  <=>  b dans l'image de A
% 5 ) d(A',b) == norm(b)  <=>  b est orthogonal à l'image de A'  <=>  b est dans le noyeau de A
