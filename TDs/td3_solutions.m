%% -------- exo 1 ---------------
n=8;
v=sqrt((2:n+1)./(1:n)); % la diagonale
% ---------------------
w=-1./v(1:end-1); % la petite diagonale
B=diag(w,-1)+diag(v)
% ---------------------
s=1:n+1:(n^2-n-1); % les indices linéaire de la diagonale (sans le dernier)
B=diag(v); B(s+1)=-1./B(s) % on recopie les elements de la diagonale vers le bas

%% -------- exo 2 ---------------
Q = [0 1;1 0]
R = [1 1;0 2]
if (Q'*Q == eye(size(Q)) && tril(R,-1)==0 && diag(R)>0)
    disp('ok')
end

%% -------- exo 3 ---------------
A = [4 2 3; 2 4 5; 3 5 7]
[sqrt(A(1,1)); A(2:end,1)/sqrt(A(1,1))]

%% -------- exo 4 ---------------
A = [4 2 0; 2 4 5; 0 5 7]
k=1
if (tril(A,-(k+1))+triu(A,(k+1)) == 0)
    disp('ok')
end
